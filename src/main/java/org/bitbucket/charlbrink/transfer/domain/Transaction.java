package org.bitbucket.charlbrink.transfer.domain;

import lombok.Builder;
import lombok.Getter;

import java.time.Instant;
import java.util.UUID;

@Getter
@Builder
public class Transaction {
    private final Instant transactionDate;
    private final UUID accountIdentification;
    private final String accountNumber;
    private final long amount;
    private final EntryType entryType;
}
