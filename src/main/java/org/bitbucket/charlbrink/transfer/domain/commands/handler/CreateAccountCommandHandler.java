package org.bitbucket.charlbrink.transfer.domain.commands.handler;

import org.bitbucket.charlbrink.transfer.domain.events.AccountEventFactory;
import org.bitbucket.charlbrink.transfer.domain.Result;
import org.bitbucket.charlbrink.transfer.domain.commands.CreateAccountCommand;
import org.bitbucket.charlbrink.transfer.domain.events.AccountOpenEvent;
import org.bitbucket.charlbrink.transfer.domain.repositories.AccountRepository;
import org.bitbucket.charlbrink.transfer.domain.InvalidAccountNumberException;
import org.bitbucket.charlbrink.transfer.infrastructure.EventBusImpl;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.util.UUID;

@Service
public class CreateAccountCommandHandler {

    private final EventBusImpl eventBus;
    private final AccountRepository accountRepository;
    private final AccountEventFactory accountEventFactory;

    @Inject
    public CreateAccountCommandHandler(final EventBusImpl eventBus, final AccountEventFactory accountEventFactory, final AccountRepository accountRepository) {
        this.eventBus = eventBus;
        this.accountEventFactory = accountEventFactory;
        this.accountRepository = accountRepository;
    }

    public Result handle(final CreateAccountCommand createAccountCommand) {
        doCreateAccount(createAccountCommand);
        return Result.SUCCESS;
    }

    private void doCreateAccount(final CreateAccountCommand createAccountCommand) {
        String accountNumber = createAccountCommand.getAccountNumber();
        if (!accountRepository.exists(accountNumber)) {
            AccountOpenEvent accountOpenEvent = accountEventFactory.open(createAccountCommand.getDateOpened(), UUID.randomUUID(), createAccountCommand.getAccountNumber(), createAccountCommand.getStartBalance());
            eventBus.post(accountOpenEvent);
        }
    }

}
