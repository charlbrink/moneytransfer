package org.bitbucket.charlbrink.transfer.domain.events.handler;

import org.bitbucket.charlbrink.transfer.domain.AccountBalance;
import org.bitbucket.charlbrink.transfer.domain.AccountBalanceView;
import org.bitbucket.charlbrink.transfer.domain.InvalidAccountNumberException;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;

@Service
public class ReadHandler {
    private final AccountBalanceView accountBalanceView;

    @Inject
    public ReadHandler(final AccountBalanceView accountBalanceView) {
        this.accountBalanceView = accountBalanceView;
    }


    public AccountBalance getAccountBalance(final String accountNumber) throws InvalidAccountNumberException {
        return accountBalanceView.getBalance(accountNumber);
    }

}
