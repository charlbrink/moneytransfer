package org.bitbucket.charlbrink.transfer.domain;

import lombok.Setter;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Setter
public class InsufficientFundsException extends Exception implements ExceptionMapper<InsufficientFundsException> {
    private String accountNumber;

    public InsufficientFundsException() {
    }

    public InsufficientFundsException(String accountNumber, String message) {
        super(message);
        this.accountNumber = accountNumber;
    }

    public InsufficientFundsException(String message, Throwable cause) {
        super(message, cause);
    }

    public InsufficientFundsException(Throwable cause) {
        super(cause);
    }

    public InsufficientFundsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public Response toResponse(InsufficientFundsException e) {
        return Response.status(Response.Status.NOT_MODIFIED).entity(e.getMessage()).build();
    }
}
