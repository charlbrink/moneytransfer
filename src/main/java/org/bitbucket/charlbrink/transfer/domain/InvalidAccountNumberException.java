package org.bitbucket.charlbrink.transfer.domain;

import lombok.Setter;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Setter
public class InvalidAccountNumberException extends Exception implements ExceptionMapper<InvalidAccountNumberException> {
    private String accountNumber;

    public InvalidAccountNumberException() {
    }

    public InvalidAccountNumberException(String accountNumber, String message) {
        super(message);
        this.accountNumber = accountNumber;
    }

    public InvalidAccountNumberException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidAccountNumberException(Throwable cause) {
        super(cause);
    }

    public InvalidAccountNumberException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public Response toResponse(InvalidAccountNumberException e) {
        return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
    }
}
