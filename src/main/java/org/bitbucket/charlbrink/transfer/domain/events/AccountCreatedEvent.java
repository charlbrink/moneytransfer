package org.bitbucket.charlbrink.transfer.domain.events;

import lombok.Value;
import org.bitbucket.charlbrink.transfer.domain.Account;

import java.time.Instant;
import java.util.UUID;

import static org.bitbucket.charlbrink.transfer.infrastructure.persistence.AccountRepositoryImpl.AGGREGATE_PREFIX;

@Value
public class AccountCreatedEvent extends DomainEvent {
    private final Account account;

    public AccountCreatedEvent(final Account account) {
        super(UUID.randomUUID(), getAggregateId(account), Instant.now());
        this.account = account;
    }

    private static String getAggregateId(final Account account) {
        return AGGREGATE_PREFIX+account.getAccountIdentification();
    }

}
