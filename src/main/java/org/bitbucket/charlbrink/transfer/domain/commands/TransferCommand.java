package org.bitbucket.charlbrink.transfer.domain.commands;

import lombok.Builder;
import lombok.Getter;

import java.time.Instant;

@Getter
@Builder
public class TransferCommand implements Command {
    private String fromAccountNumber;
    private String toAccountNumber;
    private long amount;
    private Instant when;

}
