package org.bitbucket.charlbrink.transfer.domain.repositories;

import org.bitbucket.charlbrink.transfer.domain.events.TransactionEvent;
import org.jvnet.hk2.annotations.Contract;

import java.util.UUID;

@Contract
public interface TransactionRepository {
    TransactionEvent save(TransactionEvent transactionEvent);

    long calculateBalance(UUID accountIdentification);
}
