package org.bitbucket.charlbrink.transfer.domain.repositories;

import org.bitbucket.charlbrink.transfer.domain.Account;
import org.bitbucket.charlbrink.transfer.domain.InvalidAccountNumberException;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface AccountRepository {
    String AGGREGATE_PREFIX = "Account_";

    Account create(Account account, long openingBalance);

    boolean exists(String accountNumber);

    Account find(String accountNumber) throws InvalidAccountNumberException;

    long calculateBalance(String accountNumber) throws InvalidAccountNumberException;
}
