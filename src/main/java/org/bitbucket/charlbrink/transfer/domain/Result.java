package org.bitbucket.charlbrink.transfer.domain;

public enum Result {
    ERROR, SUCCESS;
}
