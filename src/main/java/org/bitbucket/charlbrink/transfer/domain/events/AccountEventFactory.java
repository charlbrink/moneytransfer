package org.bitbucket.charlbrink.transfer.domain.events;

import org.bitbucket.charlbrink.transfer.domain.SimpleAccount;
import org.jvnet.hk2.annotations.Service;

import java.time.Instant;
import java.util.UUID;

@Service
public class AccountEventFactory {
    public AccountOpenEvent open(final Instant when, final UUID accountIdentification, final String accountNumber, final long openingBalance) {
        return buildAccountOpenEvent(when, accountIdentification, accountNumber, openingBalance);
    }

    private AccountOpenEvent buildAccountOpenEvent(final Instant when, final UUID accountIdentification, final String accountNumber, final long openingBalance) {
        return new AccountOpenEvent(SimpleAccount.builder()
                .dateOpened(when)
                .accountIdentification(accountIdentification)
                .accountNumber(accountNumber)
                .build(), openingBalance);
    }

}
