package org.bitbucket.charlbrink.transfer.domain.events;

import lombok.Value;
import org.bitbucket.charlbrink.transfer.domain.AccountBalance;

import java.time.Instant;
import java.util.UUID;

import static org.bitbucket.charlbrink.transfer.infrastructure.persistence.AccountRepositoryImpl.AGGREGATE_PREFIX;

@Value
public class AccountBalanceCalculatedEvent extends DomainEvent {
    private final AccountBalance accountBalance;

    public AccountBalanceCalculatedEvent(final AccountBalance accountBalance) {
        super(UUID.randomUUID(), getAggregateId(accountBalance), Instant.now());
        this.accountBalance = accountBalance;
    }

    private static String getAggregateId(final AccountBalance accountBalance) {
        return AGGREGATE_PREFIX+accountBalance.getAccountIdentification();
    }

}
