package org.bitbucket.charlbrink.transfer.domain.events.handler;

import com.google.common.eventbus.Subscribe;
import lombok.extern.java.Log;
import org.bitbucket.charlbrink.transfer.domain.Account;
import org.bitbucket.charlbrink.transfer.domain.AccountBalance;
import org.bitbucket.charlbrink.transfer.domain.Transaction;
import org.bitbucket.charlbrink.transfer.domain.events.AccountBalanceCalculatedEvent;
import org.bitbucket.charlbrink.transfer.domain.events.AccountCreatedEvent;
import org.bitbucket.charlbrink.transfer.domain.events.AccountOpenEvent;
import org.bitbucket.charlbrink.transfer.domain.events.DomainEvent;
import org.bitbucket.charlbrink.transfer.domain.events.TransactionEvent;
import org.bitbucket.charlbrink.transfer.domain.repositories.AccountRepository;
import org.bitbucket.charlbrink.transfer.domain.repositories.TransactionRepository;
import org.bitbucket.charlbrink.transfer.infrastructure.EventBusImpl;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;

@Log
@Service
public class DomainHandler implements EventListener {
    private final EventBusImpl eventBus;
    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;

    @Inject
    public DomainHandler(final EventBusImpl eventBus, final AccountRepository accountRepository, final TransactionRepository transactionRepository) {
        this.eventBus = eventBus;
        eventBus.register(this);
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
    }

    @Subscribe
    public void handleEvent(final DomainEvent domainEvent) {
        if (domainEvent instanceof AccountOpenEvent) {
            AccountOpenEvent accountOpenEvent = (AccountOpenEvent) domainEvent;
            log.info(String.format("Account open [%s] with balance [%d]", accountOpenEvent.getAccount().getAccountNumber(), accountOpenEvent.getOpeningBalance()));
            accountRepository.create(accountOpenEvent.getAccount(), accountOpenEvent.getOpeningBalance());
        } else if (domainEvent instanceof AccountCreatedEvent) {
            Account account = ((AccountCreatedEvent) domainEvent).getAccount();
            log.info(String.format("Account [%s] opened on [%s]", account.getAccountNumber(), account.getDateOpened()));
        } else if (domainEvent instanceof TransactionEvent) {
            TransactionEvent transactionEvent = (TransactionEvent) domainEvent;
            Transaction transaction = transactionEvent.getTransaction();
            log.info(String.format("[%s] [%s] account number [%s] with [%d]", transaction.getTransactionDate(), transaction.getEntryType(), transaction.getAccountNumber(), transaction.getAmount()));
            persistTransaction(transactionEvent);
        } else if (domainEvent instanceof AccountBalanceCalculatedEvent) {
            AccountBalance accountBalance = ((AccountBalanceCalculatedEvent) domainEvent).getAccountBalance();
            log.info(String.format("Account [%s] balance is [%s]", accountBalance.getAccountNumber(), accountBalance.getAmount()));
        }
    }

    private void persistTransaction(final TransactionEvent domainEvent) {
        transactionRepository.save(domainEvent);
    }

}
