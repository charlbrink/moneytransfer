package org.bitbucket.charlbrink.transfer.domain.commands;

import lombok.Builder;
import lombok.Getter;

import java.time.Instant;

@Getter
@Builder
public class CreateAccountCommand implements Command {
    private String accountNumber;
    private long startBalance;
    private Instant dateOpened;
}
