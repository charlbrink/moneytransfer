package org.bitbucket.charlbrink.transfer.domain;

import org.jvnet.hk2.annotations.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class AccountBalanceView {
    private Map<String, AccountBalance> balances = new HashMap<>();

    public AccountBalance getBalance(final String accountNumber) throws InvalidAccountNumberException {
        if (!balances.containsKey(accountNumber)) {
            throw new InvalidAccountNumberException(accountNumber, "No balance available for account number");
        }
        return balances.get(accountNumber);
    }

    public void setBalance(final String accountNumber, final AccountBalance accountBalance) throws InvalidAccountNumberException {
        balances.put(accountNumber, accountBalance);
    }

}
