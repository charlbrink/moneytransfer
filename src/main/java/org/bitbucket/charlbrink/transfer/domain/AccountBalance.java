package org.bitbucket.charlbrink.transfer.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AccountBalance {
    private UUID accountIdentification;
    private String accountNumber;
    private long amount;
}
