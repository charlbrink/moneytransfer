package org.bitbucket.charlbrink.transfer.domain.events;

import org.bitbucket.charlbrink.transfer.domain.EntryType;
import org.bitbucket.charlbrink.transfer.domain.Transaction;
import org.jvnet.hk2.annotations.Service;

import java.time.Instant;
import java.util.UUID;

@Service
public class TransactionEventFactory {
    public TransactionEvent debit(final Instant when, final UUID accountIdentification, final String accountNumber, final long amount) {
        return buildTransactionEvent(when, accountIdentification, accountNumber, amount, EntryType.DEBIT);
    }

    public TransactionEvent credit(final Instant when, final UUID accountIdentification, final String accountNumber, final long amount) {
        return buildTransactionEvent(when, accountIdentification, accountNumber, amount, EntryType.CREDIT);
    }

    private TransactionEvent buildTransactionEvent(final Instant when, final UUID accountIdentification, final String accountNumber, final long amount, final EntryType entryType) {
        return new TransactionEvent(Transaction.builder()
                .transactionDate(when)
                .accountIdentification(accountIdentification)
                .accountNumber(accountNumber)
                .amount(amount)
                .entryType(entryType)
                .build());
    }

}
