package org.bitbucket.charlbrink.transfer.domain;

import lombok.Builder;
import lombok.Getter;

import java.time.Instant;
import java.util.UUID;

@Getter
@Builder
public class SimpleAccount implements Account {
    private UUID accountIdentification;
    private String accountNumber;
    private Instant dateOpened;
}
