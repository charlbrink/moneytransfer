package org.bitbucket.charlbrink.transfer.domain;

public enum EntryType {
    CREDIT, DEBIT;
}
