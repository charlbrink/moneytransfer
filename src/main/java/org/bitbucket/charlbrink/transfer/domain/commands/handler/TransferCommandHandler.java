package org.bitbucket.charlbrink.transfer.domain.commands.handler;

import org.bitbucket.charlbrink.transfer.domain.Account;
import org.bitbucket.charlbrink.transfer.domain.Result;
import org.bitbucket.charlbrink.transfer.domain.commands.TransferCommand;
import org.bitbucket.charlbrink.transfer.domain.repositories.AccountRepository;
import org.bitbucket.charlbrink.transfer.domain.InsufficientFundsException;
import org.bitbucket.charlbrink.transfer.domain.InvalidAccountNumberException;
import org.bitbucket.charlbrink.transfer.domain.events.TransactionEventFactory;
import org.bitbucket.charlbrink.transfer.domain.events.TransactionEvent;
import org.bitbucket.charlbrink.transfer.infrastructure.EventBusImpl;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.util.UUID;

@Service
public class TransferCommandHandler {

    private final EventBusImpl eventBus;
    private final AccountRepository accountRepository;
    private final TransactionEventFactory transactionEventFactory;

    @Inject
    public TransferCommandHandler(final EventBusImpl eventBus, final AccountRepository accountRepository, final TransactionEventFactory transactionEventFactory) {
        this.eventBus = eventBus;
        this.accountRepository = accountRepository;
        this.transactionEventFactory = transactionEventFactory;
    }

    public Result handle(final TransferCommand transferCommand) throws InvalidAccountNumberException, InsufficientFundsException {
        doTransfer(transferCommand);
        return Result.SUCCESS;
    }


    private void doTransfer(final TransferCommand transferCommand) throws InvalidAccountNumberException, InsufficientFundsException {
        UUID fromAccountIdentification = validateAccount(transferCommand.getFromAccountNumber()).getAccountIdentification();
        UUID toAccountIdentification = validateAccount(transferCommand.getToAccountNumber()).getAccountIdentification();
        validateAvailableFunds(transferCommand.getFromAccountNumber(), transferCommand.getAmount());
        post(transactionEventFactory.credit(transferCommand.getWhen(), fromAccountIdentification, transferCommand.getFromAccountNumber(), transferCommand.getAmount()));
        post(transactionEventFactory.debit(transferCommand.getWhen(), toAccountIdentification, transferCommand.getToAccountNumber(), transferCommand.getAmount()));
    }

    private void post(final TransactionEvent transactionEvent) {
        eventBus.post(transactionEvent);
    }

    private Account validateAccount(String accountNumber) throws InvalidAccountNumberException {
        return accountRepository.find(accountNumber);
    }

    private void validateAvailableFunds(final String accountNumber, final long amount) throws InsufficientFundsException, InvalidAccountNumberException {
        long balance = accountRepository.calculateBalance(accountNumber);
        if (balance < amount) {
            throw new InsufficientFundsException(accountNumber, "Insufficient funds available");
        }
    }
}
