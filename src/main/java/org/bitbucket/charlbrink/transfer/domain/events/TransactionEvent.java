package org.bitbucket.charlbrink.transfer.domain.events;

import lombok.Value;
import org.bitbucket.charlbrink.transfer.domain.Transaction;

import java.time.Instant;
import java.util.UUID;

import static org.bitbucket.charlbrink.transfer.infrastructure.persistence.AccountRepositoryImpl.AGGREGATE_PREFIX;

@Value
public class TransactionEvent extends DomainEvent {
    private final Transaction transaction;

    public TransactionEvent(final Transaction transaction) {
        super(UUID.randomUUID(), getAggregateId(transaction), Instant.now());
        this.transaction = transaction;
    }

    private static String getAggregateId(final Transaction transaction) {
        return AGGREGATE_PREFIX+transaction.getAccountIdentification();
    }

}
