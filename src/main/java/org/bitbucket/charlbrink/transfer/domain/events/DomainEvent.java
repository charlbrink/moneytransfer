package org.bitbucket.charlbrink.transfer.domain.events;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Instant;
import java.util.UUID;

@Getter
@AllArgsConstructor
public abstract class DomainEvent {
    private final UUID eventId;
    private final String aggregateId;
    private final Instant when;
}
