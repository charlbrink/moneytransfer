package org.bitbucket.charlbrink.transfer.domain;

import java.time.Instant;
import java.util.UUID;

public interface Account {
    Instant getDateOpened();
    UUID getAccountIdentification();
    String getAccountNumber();
}
