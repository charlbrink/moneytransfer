package org.bitbucket.charlbrink.transfer.domain.events.handler;

import com.google.common.eventbus.Subscribe;
import lombok.extern.java.Log;
import org.bitbucket.charlbrink.transfer.domain.AccountBalance;
import org.bitbucket.charlbrink.transfer.domain.AccountBalanceView;
import org.bitbucket.charlbrink.transfer.domain.InvalidAccountNumberException;
import org.bitbucket.charlbrink.transfer.domain.events.AccountBalanceCalculatedEvent;
import org.bitbucket.charlbrink.transfer.domain.events.DomainEvent;
import org.bitbucket.charlbrink.transfer.infrastructure.EventBusImpl;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;

@Log
@Service
public class ViewHandler implements EventListener {
    private final AccountBalanceView accountBalanceView;

    @Inject
    public ViewHandler(final EventBusImpl eventBus, final AccountBalanceView accountBalanceView) {
        this.accountBalanceView = accountBalanceView;
        eventBus.register(this);
    }

    @Subscribe
    public void handleEvent(final DomainEvent domainEvent) {
        if (domainEvent instanceof AccountBalanceCalculatedEvent) {
            try {
                updateView((AccountBalanceCalculatedEvent) domainEvent);
            } catch (InvalidAccountNumberException e) {
                log.warning(String.format("Event ignored [%s]", domainEvent));
            }
        }
    }

    private void updateView(final AccountBalanceCalculatedEvent accountBalanceCalculatedEvent) throws InvalidAccountNumberException {
        AccountBalance accountBalance = accountBalanceCalculatedEvent.getAccountBalance();
        accountBalanceView.setBalance(accountBalance.getAccountNumber(),accountBalance);
        log.info(String.format("Balance updated [%s] [%s]", accountBalance.getAccountNumber(), accountBalance.getAmount()));
    }

}
