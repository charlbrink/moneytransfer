package org.bitbucket.charlbrink.transfer;

import lombok.extern.java.Log;
import org.bitbucket.charlbrink.transfer.domain.InsufficientFundsException;
import org.bitbucket.charlbrink.transfer.domain.InvalidAccountNumberException;
import org.bitbucket.charlbrink.transfer.infrastructure.web.AccountResource;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;

@Log
public class App {

    public static final URI BASE_URI = URI.create("http://localhost:8080/moneytransfer/");

    public static void main(final String[] args) {

        try {
            final HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, create(), true);
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    server.shutdownNow();
                }
            }));
            server.start();

            log.info(String.format("Application started.%nStop the application using CTRL+C"));

            Thread.currentThread().join();
        } catch (InterruptedException | IOException ex) {
            log.log(Level.SEVERE, null, ex);
        }

    }

    public static ResourceConfig create() {
        final ResourceConfig resourceConfig = new ResourceConfig(
                AccountResource.class,
                JacksonFeature.class,
                InvalidAccountNumberException.class,
                InsufficientFundsException.class);
        resourceConfig.register(new MoneyTransferBinder());
        return resourceConfig;
    }

}