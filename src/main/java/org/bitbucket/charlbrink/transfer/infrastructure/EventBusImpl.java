package org.bitbucket.charlbrink.transfer.infrastructure;

import com.google.common.eventbus.EventBus;
import org.bitbucket.charlbrink.transfer.domain.events.DomainEvent;
import org.bitbucket.charlbrink.transfer.domain.events.handler.EventListener;
import org.jvnet.hk2.annotations.Service;

@Service
public class EventBusImpl {

    private final EventBus eventBus = new EventBus("moneytransfer");

    public void post(final DomainEvent domainEvent) {
        eventBus.post(domainEvent);
    }

    public void register(final EventListener eventListener) {
        eventBus.register(eventListener);
    }

}
