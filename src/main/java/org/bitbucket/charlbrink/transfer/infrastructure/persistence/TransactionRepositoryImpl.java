package org.bitbucket.charlbrink.transfer.infrastructure.persistence;

import org.bitbucket.charlbrink.transfer.domain.AccountBalance;
import org.bitbucket.charlbrink.transfer.domain.EntryType;
import org.bitbucket.charlbrink.transfer.domain.Transaction;
import org.bitbucket.charlbrink.transfer.domain.repositories.TransactionRepository;
import org.bitbucket.charlbrink.transfer.domain.events.AccountBalanceCalculatedEvent;
import org.bitbucket.charlbrink.transfer.domain.events.DomainEvent;
import org.bitbucket.charlbrink.transfer.domain.events.TransactionEvent;
import org.bitbucket.charlbrink.transfer.infrastructure.EventBusImpl;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;

import static org.bitbucket.charlbrink.transfer.infrastructure.persistence.AccountRepositoryImpl.AGGREGATE_PREFIX;

@Service
public class TransactionRepositoryImpl implements TransactionRepository {

    private final EventBusImpl eventBus;
    private final EventStore eventStore;

    @Inject
    public TransactionRepositoryImpl(final EventBusImpl eventBus, final EventStore eventStore) {
        this.eventBus = eventBus;
        this.eventStore = eventStore;
    }

    @Override
    public TransactionEvent save(final TransactionEvent transactionEvent) {
        eventStore.persistEvent(transactionEvent);
        updateBalance(transactionEvent);
        return transactionEvent;
    }

    private void updateBalance(final TransactionEvent transactionEvent) {
        Transaction transaction = transactionEvent.getTransaction();
        long balance = calculateBalance(transactionEvent.getTransaction().getAccountIdentification());

        AccountBalance accountBalance = new AccountBalance(transaction.getAccountIdentification(), transaction.getAccountNumber(), balance);
        eventBus.post(new AccountBalanceCalculatedEvent(accountBalance));
    }

    @Override
    public long calculateBalance(final UUID accountIdentification) {
        long balance = 0;
        List<DomainEvent> accountDomainEvents = eventStore.getEvents(getAggregateIdentification(accountIdentification));
        balance = accountDomainEvents.stream().filter(event -> event instanceof TransactionEvent).mapToLong(event -> getAmount((TransactionEvent)event)).sum();
        return balance;
    }

    private long getAmount(final TransactionEvent event) {
        return event.getTransaction().getEntryType() == EntryType.CREDIT ? 0 - event.getTransaction().getAmount() : event.getTransaction().getAmount();
    }

    private String getAggregateIdentification(final UUID accountIdentification) {
        return AGGREGATE_PREFIX+accountIdentification;
    }

}
