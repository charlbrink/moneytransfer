package org.bitbucket.charlbrink.transfer.infrastructure.web;

public class CreateAccountRequest {
    private String accountNumber;
    private long startBalance;
    private String dateOpened; //ISO_INSTANT 2011-12-03T10:15:30Z

    public CreateAccountRequest() {
    }

    public CreateAccountRequest(final String accountNumber, final long startBalance, final String dateOpened) {
        this.accountNumber = accountNumber;
        this.startBalance = startBalance;
        this.dateOpened = dateOpened;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public long getStartBalance() {
        return startBalance;
    }

    public void setStartBalance(long startBalance) {
        this.startBalance = startBalance;
    }

    public String getDateOpened() {
        return dateOpened;
    }

    public void setDateOpened(String dateOpened) {
        this.dateOpened = dateOpened;
    }
}
