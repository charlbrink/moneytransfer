package org.bitbucket.charlbrink.transfer.infrastructure.persistence;

import org.bitbucket.charlbrink.transfer.domain.Account;
import org.bitbucket.charlbrink.transfer.domain.InvalidAccountNumberException;
import org.bitbucket.charlbrink.transfer.domain.SimpleAccount;
import org.bitbucket.charlbrink.transfer.domain.events.TransactionEventFactory;
import org.bitbucket.charlbrink.transfer.domain.events.AccountCreatedEvent;
import org.bitbucket.charlbrink.transfer.domain.repositories.AccountRepository;
import org.bitbucket.charlbrink.transfer.domain.repositories.TransactionRepository;
import org.bitbucket.charlbrink.transfer.infrastructure.EventBusImpl;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class AccountRepositoryImpl implements AccountRepository {

    private final Map<String, UUID> accountTable = new HashMap<>();
    private final EventBusImpl eventBus;
    private final TransactionEventFactory transactionEventFactory;
    private final TransactionRepository transactionRepository;

    @Inject
    public AccountRepositoryImpl(final EventBusImpl eventBus, final TransactionEventFactory transactionEventFactory, final TransactionRepository transactionRepository) {
        this.eventBus = eventBus;
        this.transactionEventFactory = transactionEventFactory;
        this.transactionRepository = transactionRepository;
    }

    @Override
    public Account create(final Account account, final long openingBalance) {
        accountTable.put(account.getAccountNumber(), account.getAccountIdentification());
        SimpleAccount persistedAccount = SimpleAccount.builder()
                .accountIdentification(account.getAccountIdentification())
                .accountNumber(account.getAccountNumber())
                .dateOpened(account.getDateOpened())
                .build();
        eventBus.post(new AccountCreatedEvent(persistedAccount));
        eventBus.post(transactionEventFactory.debit(persistedAccount.getDateOpened(), persistedAccount.getAccountIdentification(), persistedAccount.getAccountNumber(), openingBalance));
        return account;
    }

    @Override
    public boolean exists(final String accountNumber) {
        Optional<UUID> accountIdentification = findAccountIdentification(accountNumber);
        return accountIdentification.isPresent();
    }

    @Override
    public Account find(final String accountNumber) throws InvalidAccountNumberException {
        Optional<UUID> accountIdentification = findAccountIdentification(accountNumber);
        if (!accountIdentification.isPresent()) {
            throw new InvalidAccountNumberException(accountNumber, String.format("Invalid account [%s]", accountNumber));
        }

        Account account = SimpleAccount.builder()
                .accountIdentification(accountIdentification.get())
                .accountNumber(accountNumber)
                .build();

        return account;
    }

    @Override
    public long calculateBalance(final String accountNumber) throws InvalidAccountNumberException {
        Optional<UUID> accountIdentification = findAccountIdentification(accountNumber);
        if (!accountIdentification.isPresent()) {
            throw new InvalidAccountNumberException(accountNumber, String.format("Invalid account [%s]", accountNumber));
        }

        return transactionRepository.calculateBalance(accountIdentification.get());
    }

    private Optional<UUID> findAccountIdentification(final String accountNumber) {
        return Optional.ofNullable(accountTable.get(accountNumber));
    }

}
