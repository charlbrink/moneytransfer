package org.bitbucket.charlbrink.transfer.infrastructure.web;

import org.bitbucket.charlbrink.transfer.domain.InsufficientFundsException;
import org.bitbucket.charlbrink.transfer.domain.InvalidAccountNumberException;
import org.bitbucket.charlbrink.transfer.domain.commands.CreateAccountCommand;
import org.bitbucket.charlbrink.transfer.domain.commands.TransferCommand;
import org.bitbucket.charlbrink.transfer.domain.commands.handler.CreateAccountCommandHandler;
import org.bitbucket.charlbrink.transfer.domain.Result;
import org.bitbucket.charlbrink.transfer.domain.commands.handler.TransferCommandHandler;
import org.bitbucket.charlbrink.transfer.domain.events.handler.ReadHandler;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.time.Instant;
import java.time.format.DateTimeFormatter;

@Service
@Path("/accounts")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

    private final TransferCommandHandler transferCommandHandler;
    private final CreateAccountCommandHandler createAccountCommandHandler;
    private final ReadHandler readHandler;

    @Inject
    public AccountResource(final TransferCommandHandler transferCommandHandler, final CreateAccountCommandHandler createAccountCommandHandler, final ReadHandler readHandler) {
        this.transferCommandHandler = transferCommandHandler;
        this.createAccountCommandHandler = createAccountCommandHandler;
        this.readHandler = readHandler;
    }

    /**
     * Create account with opening balance (idempotent)
     * @param createAccountRequest
     * @return
     * @throws InvalidAccountNumberException
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAccount(CreateAccountRequest createAccountRequest) throws InvalidAccountNumberException {
        CreateAccountCommand createAccountCommand = CreateAccountCommand.builder()
                .accountNumber(createAccountRequest.getAccountNumber())
                .dateOpened(Instant.from(DateTimeFormatter.ISO_INSTANT.parse(createAccountRequest.getDateOpened())))
                .startBalance(createAccountRequest.getStartBalance())
                .build();
        Result result = createAccountCommandHandler.handle(createAccountCommand);
        if (result == Result.SUCCESS) {
            return Response.status(Response.Status.CREATED).location(URI.create("/accounts/" + createAccountCommand.getAccountNumber())).build();
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Unknown error").build();
        }
    }

    /**
     * Make transfer between accounts
     * @param accountNumber
     * @param transferRequest
     * @throws InvalidAccountNumberException
     * @throws InsufficientFundsException
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{accountNumber}")
    public void transferTo(@PathParam("accountNumber") String accountNumber, TransferRequest transferRequest) throws InvalidAccountNumberException, InsufficientFundsException {
        TransferCommand transferCommand = TransferCommand.builder()
                .when(Instant.from(DateTimeFormatter.ISO_INSTANT.parse(transferRequest.getTransferDate())))
                .fromAccountNumber(accountNumber)
                .toAccountNumber(transferRequest.getToAccountNumber())
                .amount(transferRequest.getAmount())
                .build();
        transferCommandHandler.handle(transferCommand);
    }

    /**
     * Get balance from view
     * @param accountNumber
     * @return
     * @throws InvalidAccountNumberException
     */
    @GET
    @Path("/{accountNumber}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBalance(@PathParam("accountNumber") String accountNumber) throws InvalidAccountNumberException {
        return Response.ok(readHandler.getAccountBalance(accountNumber)).build();
    }

}
