package org.bitbucket.charlbrink.transfer.infrastructure.web;

public class TransferRequest {
    private String toAccountNumber;
    private long amount;
    private String transferDate; //ISO_INSTANT 2011-12-03T10:15:30Z

    public TransferRequest() {
    }

    public TransferRequest(final String toAccountNumber, final long amount, final String transferDate) {
        this.toAccountNumber = toAccountNumber;
        this.amount = amount;
        this.transferDate = transferDate;
    }

    public String getToAccountNumber() {
        return toAccountNumber;
    }

    public void setToAccountNumber(String toAccountNumber) {
        this.toAccountNumber = toAccountNumber;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(String transferDate) {
        this.transferDate = transferDate;
    }
}
