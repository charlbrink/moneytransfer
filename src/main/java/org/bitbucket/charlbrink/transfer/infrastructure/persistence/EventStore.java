package org.bitbucket.charlbrink.transfer.infrastructure.persistence;

import org.bitbucket.charlbrink.transfer.domain.events.DomainEvent;
import org.jvnet.hk2.annotations.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EventStore {
    Map<String, List<DomainEvent>> events = new HashMap<>();

    public EventStore() {
    }

    public List<DomainEvent> getEvents(final String aggregateId) {
        return events.get(aggregateId);
    }

    public void persistEvent(final DomainEvent domainEvent) {
        if (!events.containsKey(domainEvent.getAggregateId())) {
            events.put(domainEvent.getAggregateId(), new ArrayList<>());
        }
        events.get(domainEvent.getAggregateId()).add(domainEvent);
    }
}
