package org.bitbucket.charlbrink.transfer;

import org.bitbucket.charlbrink.transfer.domain.commands.handler.CreateAccountCommandHandler;
import org.bitbucket.charlbrink.transfer.domain.commands.handler.TransferCommandHandler;
import org.bitbucket.charlbrink.transfer.domain.events.handler.DomainHandler;
import org.bitbucket.charlbrink.transfer.infrastructure.EventBusImpl;
import org.bitbucket.charlbrink.transfer.domain.events.handler.ReadHandler;
import org.bitbucket.charlbrink.transfer.domain.events.handler.ViewHandler;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

public class MoneyTransferBinder extends AbstractBinder {

    @Override
    protected void configure() {
        ServiceLocator locator = ServiceLocatorUtilities.createAndPopulateServiceLocator();
        bind(locator.createAndInitialize(EventBusImpl.class));
        bind(locator.createAndInitialize(DomainHandler.class));
        bind(locator.createAndInitialize(ViewHandler.class));
        bind(locator.createAndInitialize(ReadHandler.class));
        bind(locator.createAndInitialize(TransferCommandHandler.class));
        bind(locator.createAndInitialize(CreateAccountCommandHandler.class));
    }

}
