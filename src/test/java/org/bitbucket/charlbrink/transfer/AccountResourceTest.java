package org.bitbucket.charlbrink.transfer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.java.Log;
import org.bitbucket.charlbrink.transfer.domain.AccountBalance;
import org.bitbucket.charlbrink.transfer.infrastructure.web.CreateAccountRequest;
import org.bitbucket.charlbrink.transfer.infrastructure.web.TransferRequest;
import org.glassfish.grizzly.http.util.HttpStatus;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.logging.Level;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@Log
public class AccountResourceTest extends JerseyTest {

    @Override
    protected ResourceConfig configure() {
        return App.create();
    }

    private WebTarget prepareTarget(String path) {
        final WebTarget target = target();
        target.register(LoggingFeature.class);
        return target.path(path);
    }

    @Test
    public void givenAccountResource_whenCreatingAccountAgain_thenExpectSuccess() {
        //create account
        CreateAccountRequest account100_1 = new CreateAccountRequest("100", 500, "2019-10-03T10:15:30Z");
        Entity<CreateAccountRequest> account100_1Json = Entity.json(account100_1);

        assertThat("Expect location to match", prepareTarget("accounts").request().put(account100_1Json).getLocation().getPath(), is(equalTo("/accounts/100")));

        //create account
        CreateAccountRequest account100_2 = new CreateAccountRequest("100", 500, "2019-10-03T10:20:30Z");
        Entity<CreateAccountRequest> account100_2Json = Entity.json(account100_2);
        assertThat("Expect location to match", prepareTarget("accounts").request().put(account100_2Json).getLocation().getPath(), is(equalTo("/accounts/100")));

        //validate balances
        assertThat("Expect starting balance to match", prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "100").request(MediaType.APPLICATION_JSON).get(AccountBalance.class).getAmount(), is(equalTo(500L)));
    }

    @Test
    public void givenAccountResource_whenCreatingAccountsAndMakingTransfer_thenExpectSuccess() {
        //create account
        CreateAccountRequest account100 = new CreateAccountRequest("100", 500, "2019-10-03T10:15:30Z");
        Entity<CreateAccountRequest> account100Json = Entity.json(account100);
        assertThat("Expect location to match", prepareTarget("accounts").request().put(account100Json).getLocation().getPath(), is(equalTo("/accounts/100")));

        //create account
        CreateAccountRequest account200 = new CreateAccountRequest("200", 500, "2019-10-03T10:20:30Z");
        Entity<CreateAccountRequest> account200Json = Entity.json(account200);
        assertThat("Expect location to match", prepareTarget("accounts").request().put(account200Json).getLocation().getPath(), is(equalTo("/accounts/200")));

        //validate balances
        assertThat("Expect starting balance to match", prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "100").request(MediaType.APPLICATION_JSON).get(AccountBalance.class).getAmount(), is(equalTo(500L)));
        assertThat("Expect starting balance to match", prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "200").request(MediaType.APPLICATION_JSON).get(AccountBalance.class).getAmount(), is(equalTo(500L)));

        //make transfer
        TransferRequest transferRequest = new TransferRequest("200", 50, "2019-10-04T10:15:30Z");
        Entity<TransferRequest> transferRequestJson = Entity.json(transferRequest);
        Response response = prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "100").request().put(transferRequestJson);
        assertThat("Expect transfer to succeed", response.getStatus(), is(equalTo(HttpStatus.NO_CONTENT_204.getStatusCode())));
        assertThat("Expect transfer to succeed", response.getStatusInfo().getStatusCode(), is(equalTo(HttpStatus.NO_CONTENT_204.getStatusCode())));
        assertThat("Expect transfer to succeed", response.getStatusInfo().getReasonPhrase(), is(equalTo(HttpStatus.NO_CONTENT_204.getReasonPhrase())));

        //validate balances after transfer
        assertThat("Expect ending balance to match", prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "100").request(MediaType.APPLICATION_JSON).get(AccountBalance.class).getAmount(), is(equalTo(450L)));
        assertThat("Expect ending balance to match", prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "200").request(MediaType.APPLICATION_JSON).get(AccountBalance.class).getAmount(), is(equalTo(550L)));
    }

    @Test
    public void givenAccountResource_whenAttemptToTransferMoreThanAvailable_thenExpectFailure() {
        //create account
        CreateAccountRequest account100 = new CreateAccountRequest("100", 500, "2019-10-03T10:15:30Z");
        Entity<CreateAccountRequest> account100Json = Entity.json(account100);
        assertThat("Expect location to match", prepareTarget("accounts").request().put(account100Json).getLocation().getPath(), is(equalTo("/accounts/100")));

        //create account
        CreateAccountRequest account200 = new CreateAccountRequest("200", 500, "2019-10-03T10:20:30Z");
        Entity<CreateAccountRequest> account200Json = Entity.json(account200);
        assertThat("Expect location to match", prepareTarget("accounts").request().put(account200Json).getLocation().getPath(), is(equalTo("/accounts/200")));

        //make transfer
        TransferRequest transferRequest = new TransferRequest("200", 501, "2019-10-04T10:15:30Z");
        Entity<TransferRequest> transferRequestJson = Entity.json(transferRequest);
        Response response = prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "100").request().put(transferRequestJson);
        assertThat("Expect transfer to succeed", response.getStatus(), is(equalTo(HttpStatus.NOT_MODIFIED_304.getStatusCode())));
        assertThat("Expect transfer to succeed", response.getStatusInfo().getStatusCode(), is(equalTo(HttpStatus.NOT_MODIFIED_304.getStatusCode())));
        assertThat("Expect transfer to succeed", response.getStatusInfo().getReasonPhrase(), is(equalTo(HttpStatus.NOT_MODIFIED_304.getReasonPhrase())));

        //validate balances after transfer
        assertThat("Expect ending balance to match", prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "100").request(MediaType.APPLICATION_JSON).get(AccountBalance.class).getAmount(), is(equalTo(500L)));
        assertThat("Expect ending balance to match", prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "200").request(MediaType.APPLICATION_JSON).get(AccountBalance.class).getAmount(), is(equalTo(500L)));
    }

    @Test
    public void givenAccountResource_whenGetBalanceForInvalidAccount_thenExpectFailure() {
        assertThat("Expect failure", prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "100").request(MediaType.APPLICATION_JSON).get().getStatus(), is(equalTo(HttpStatus.BAD_REQUEST_400.getStatusCode())));
        assertThat("Expect failure", prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "200").request(MediaType.APPLICATION_JSON).get().getStatus(), is(equalTo(HttpStatus.BAD_REQUEST_400.getStatusCode())));
    }

    @Test
    public void givenAccountResource_whenAttemptToTransferFromOrToInvalidAccount_thenExpectFailure() {
        //create account
        CreateAccountRequest account100 = new CreateAccountRequest("100", 500, "2019-10-03T10:15:30Z");
        showJson(account100);
        Entity<CreateAccountRequest> account100Json = Entity.json(account100);
        assertThat("Expect location to match", prepareTarget("accounts").request().put(account100Json).getLocation().getPath(), is(equalTo("/accounts/100")));

        //make transfer to invalid account
        TransferRequest transferRequest1 = new TransferRequest("200", 50, "2019-10-04T10:15:30Z");
        showJson(transferRequest1);
        Entity<TransferRequest> transferRequestJson1 = Entity.json(transferRequest1);
        Response response1 = prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "100").request().put(transferRequestJson1);
        assertThat("Expect transfer to fail", response1.getStatus(), is(equalTo(HttpStatus.BAD_REQUEST_400.getStatusCode())));

        //make transfer from invalid account
        TransferRequest transferRequest2 = new TransferRequest("100", 50, "2019-10-04T10:15:30Z");
        Entity<TransferRequest> transferRequestJson2 = Entity.json(transferRequest2);
        Response response2 = prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "200").request().put(transferRequestJson2);
        assertThat("Expect transfer to fail", response2.getStatus(), is(equalTo(HttpStatus.BAD_REQUEST_400.getStatusCode())));

        //validate balances after transfer
        assertThat("Expect ending balance to match", prepareTarget("accounts/{accountNumber}").resolveTemplate("accountNumber", "100").request(MediaType.APPLICATION_JSON).get(AccountBalance.class).getAmount(), is(equalTo(500L)));
    }

    private void showJson(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        log.log(Level.FINE,jsonInString);
    }
}