package org.bitbucket.charlbrink.transfer.infrastructure;

import org.bitbucket.charlbrink.transfer.domain.AccountBalance;
import org.bitbucket.charlbrink.transfer.domain.AccountBalanceView;
import org.bitbucket.charlbrink.transfer.domain.InvalidAccountNumberException;
import org.bitbucket.charlbrink.transfer.domain.events.AccountBalanceCalculatedEvent;
import org.bitbucket.charlbrink.transfer.domain.events.handler.ViewHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ViewHandlerTest {

    @Mock
    EventBusImpl eventBus;

    @Mock
    private AccountBalanceView accountBalanceView;

    @InjectMocks
    private ViewHandler viewHandler;

    @Test
    public void givenAccountBalanceCalculatedEvent_whenHandleEvent_thenExpectBalanceUpdated() throws InvalidAccountNumberException {
        AccountBalanceCalculatedEvent accountBalanceCalculatedEvent = new AccountBalanceCalculatedEvent(new AccountBalance(UUID.randomUUID(), "100", 500));
        viewHandler.handleEvent(accountBalanceCalculatedEvent);

        verify(accountBalanceView, times(1)).setBalance(anyString(), any(AccountBalance.class));
    }

    @Test
    public void givenTransactionEvent_whenHandleEvent_thenExpectBalanceNotUpdated() throws InvalidAccountNumberException {
        AccountBalanceCalculatedEvent accountBalanceCalculatedEvent = new AccountBalanceCalculatedEvent(new AccountBalance());
        viewHandler.handleEvent(accountBalanceCalculatedEvent);

        verify(accountBalanceView, never()).setBalance(anyString(), any(AccountBalance.class));
    }

}