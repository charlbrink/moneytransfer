package org.bitbucket.charlbrink.transfer.infrastructure.persistence;

import org.bitbucket.charlbrink.transfer.domain.SimpleAccount;
import org.bitbucket.charlbrink.transfer.domain.events.AccountCreatedEvent;
import org.bitbucket.charlbrink.transfer.domain.events.DomainEvent;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class EventStoreTest {
    private EventStore eventStore;

    @Before
    public void setup() {
        eventStore = new EventStore();
    }

    @Test
    public void givenDomainEvent_whenPersist_thenCanRetrieve() {
        DomainEvent event = new AccountCreatedEvent(SimpleAccount.builder().build());

        eventStore.persistEvent(event);

        assertFalse(eventStore.getEvents(event.getAggregateId()).isEmpty());
        assertEquals("Expect one event ", 1, eventStore.getEvents(event.getAggregateId()).size());
    }
}