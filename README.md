#Money Transfer Backend

##Overview
Simple implementation of a money transfer REST API using events.
Based on https://medium.com/hackernoon/1-year-of-event-sourcing-and-cqrs-fb9033ccd1c6. 

![Diagram showing application design](https://miro.medium.com/max/700/0*8oBk88QHJc00FHbL.)

##Running
mvn exec:java
